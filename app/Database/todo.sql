drop database if EXISTS todo;

create database todo;

use todo;

create table user (
    id int primary key auto_increment,
    username varchar(30) not NULL UNIQUE,
    password varchar(255) not null,
    firstname varchar(100),
    lastname varchar(100)
);

create table task (
    id int PRIMARY KEY auto_increment,
    title varchar(255) not null,
    added TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    description text,
    user_id int not null,
    index (user_id),
    FOREIGN KEY (user_id) REFERENCES user(id)
    on delete RESTRICT
);